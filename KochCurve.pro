#-------------------------------------------------
#
# Project created by QtCreator 2016-06-27T00:50:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KochCurve
TEMPLATE = app


SOURCES += code/main.cpp\
	code/MainWindow.cpp \
	code/KochCurve.cpp \
	code/KochWidget.cpp \
	code/FilenameDialog.cpp

HEADERS  += code/MainWindow.h \
	code/KochCurve.h \
	code/KochWidget.h \
	code/FilenameDialog.h
