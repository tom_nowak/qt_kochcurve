The program draws Koch Curve in an interactive way. You can also save picture of the curve.
I wrote this project before I started using GIT, so commit history is not interesting...

Documenting mathematical equations used to derive coordinates would be difficult, so I did not do it - you can look it up elsewhere.

Notice that class KochCurve has nothing to do with Qt - it can be used in other programs as well. Similarly, class KochWidget has nothing to do with the way of calculating coordinates - it can be used to display any polygonal chain (after adjusting signals to change some parameters).

Many things could be changed, for example refactorization of MainWindow constructor (too long function, however it is divided in sections by comments), or changing limits of coordinates/radius accoring to window size (now it is 'hardcoded').
