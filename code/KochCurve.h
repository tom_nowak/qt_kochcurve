#pragma once

#include <forward_list>

//
// The core of Koch Curve - class containing list of points used
// to describe the curve. There are methods to edit the curve, but
// there are no functions connected with graphical interface or Qt
// (so this class can be used in different programs, including console ones).
//


struct Point
{
    float x;
    float y;
    Point() {}
    Point(float xx, float yy) : x(xx), y(yy) {}
};


typedef std::forward_list<Point>::const_iterator PointIterator;


class KochCurve
{
public:
    KochCurve();

    inline int getIterationsNumber() { return iterationsNumber; }
    inline Point getCenter() { return center; }
    inline float getRadius() { return radius; }
    inline double getRotation() { return rotation; }
    inline PointIterator getIteratorBegin() { return pointsList.begin(); }
    inline PointIterator getIteratorEnd() { return pointsList.end(); }

    void setIterationsNumber(int);
    void setCenter(Point);
    void setCenterX(float);
    void setCenterY(float);
    void setRadius(float);
    void setRotation(float);

private:
    int iterationsNumber;
    std::forward_list<Point> pointsList;
    Point center; // coordinates of the center of the circle described on the curve
    float radius; // radius of the circle described on the curve
    double rotation;

    void addSpike(PointIterator, PointIterator); // add 'spike' (three new points)
    void addIteration(); // adds 'spikes' between each pair of points
    void substractIteration(); // deletes 'spikes' from the previous iteration
};
