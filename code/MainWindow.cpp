#include "MainWindow.h"
#include "KochWidget.h"
#include "FilenameDialog.h"

#include <QBoxLayout>
#include <QGridLayout>
#include <QSlider>
#include <QSpinBox>
#include <QLabel>
#include <QFont>
#include <QToolBar>
#include <QAction>
#include <QIcon>
#include <QDebug>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    /////////////////////////////////////////////////////////////////////////////
    // Creating toolbar and setting general window attributes:
    //qDebug("creating toolbar and setting general window attributes");

    setMinimumSize(750,500);
    setWindowTitle(tr("Koch curve editor"));
    QIcon icon("icon.png");
    QToolBar *toolbar = addToolBar("main toolbar");
    QAction *exportImage = toolbar->addAction(icon, "Export image");
    QWidget *mainWidget = new QWidget(this);
    setCentralWidget(mainWidget);

    //// End of creating toolbar and setting general window attributes
    /////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////
    // Creating new objects for pointers in the class:
    ////qDebug("creating new objects");

    QBoxLayout *boxLayout = new QBoxLayout(QBoxLayout::LeftToRight, mainWidget);
    kochWidget = new KochWidget(this);
    QGridLayout *layout = new QGridLayout(mainWidget);
    labels = new QLabel[10];
    iterationsRegulation = new QSpinBox(this);
    xCenterRegulation = new QSlider(Qt::Vertical, this);
    yCenterRegulation = new QSlider(Qt::Vertical, this);
    radiusRegulation = new QSlider(Qt::Horizontal, this);
    rotationRegulation = new QSlider(Qt::Horizontal, this);
    colourRegulation = new QSlider[3];
    filenameDialog = new FilenameDialog(mainWidget);
    messageBox = new QMessageBox(this);

    //// End of creating new objects for pointers in the class
    /////////////////////////////////////////////////////////////////////////////
    
    
    /////////////////////////////////////////////////////////////////////////////
    // Setting text attributes:
    ////qDebug("setting text attributes");

    QFont font;
    font.setPointSize(12);
    this->setFont(font);
    for(int i=0; i<10; ++i)
        labels[i].setAlignment(Qt::AlignCenter);

    messageBox->setWindowTitle(tr("Export image alert"));

    //// End of setting text attributes
    /////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////
    // Adding widgets to the grid layout:
    ////qDebug("adding widgets to the grid layout");

    labels[0].setText(tr("Number of iterations"));
    layout->addWidget(labels+0, 0, 0, 1, 2);
    iterationsRegulation->setRange(0, 8);
    layout->addWidget(iterationsRegulation, 0, 2, 1, 1);

    labels[1].setText(tr("Center coordinates"));
    layout->addWidget(labels+1, 1, 0, 1, 3);
    labels[2].setText(tr("x"));
    labels[3].setText(tr("y"));
    layout->addWidget(labels+2, 2, 0, 1, 1);
    layout->addWidget(labels+3, 2, 2, 1, 1);
    xCenterRegulation->setRange(-1000, 1000);
    xCenterRegulation->setValue(100);
    yCenterRegulation->setRange(-1000, 1000);
    yCenterRegulation->setValue(100);
    layout->addWidget(xCenterRegulation, 3, 0, 1, 1);
    layout->addWidget(yCenterRegulation, 3, 2, 1, 1);

    labels[4].setText(tr("Radius"));
    layout->addWidget(labels+4, 4, 0, 1, 3);
    radiusRegulation->setRange(1, 1000);
    radiusRegulation->setValue(100);
    layout->addWidget(radiusRegulation, 5, 0, 1, 3);

    labels[5].setText(tr("Rotation"));
    layout->addWidget(labels+5, 6, 0, 1, 3);
    rotationRegulation->setRange(0,360);
    layout->addWidget(rotationRegulation, 7, 0, 1, 3);

    labels[6].setText(tr("Colour parameters"));
    layout->addWidget(labels+6, 8, 0, 1, 3);
    labels[7].setText(tr("Red"));
    labels[8].setText(tr("Green"));
    labels[9].setText(tr("Blue"));
    for(int i=0; i<3; ++i)
    {
        colourRegulation[i].setRange(0,255);
        colourRegulation[i].setOrientation(Qt::Vertical);
        layout->addWidget(labels+7+i, 9, i, 1, 1);
        layout->addWidget(colourRegulation+i, 10, i, 1, 1);
    }

    //// End of adding widgets to the grid layout
    /////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////
    // Adding items to the box layout:
    ////qDebug("adding items to the box layout");

    boxLayout->addWidget(kochWidget, 3);
    boxLayout->addLayout(layout, 1);
    this->setLayout(boxLayout);

    //// End of adding items to the box layout
    /////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////
    // Connecting signals with slots:
    ////qDebug("connecting signals with slots");

    connect(iterationsRegulation, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeIterations(int)));
    connect(xCenterRegulation, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeCenterX(int)));
    connect(yCenterRegulation, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeCenterY(int)));
    connect(radiusRegulation, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeRadius(int)));
    connect(rotationRegulation, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeRotation(int)));
    connect(colourRegulation+0, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeRed(int)));
    connect(colourRegulation+1, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeGreen(int)));
    connect(colourRegulation+2, SIGNAL(valueChanged(int)), kochWidget, SLOT(changeBlue(int)));

    connect(exportImage, SIGNAL(triggered()), this, SLOT(openFilenameDialog()));
    connect(filenameDialog, SIGNAL(signalImageToSave(QString)), this, SLOT(exportImage(QString)));

    //// End of connecting signals with slots
    /////////////////////////////////////////////////////////////////////////////
}


MainWindow::~MainWindow()
{
    // These ones have not been given parent 'this', so they are deleted below:
    delete [] colourRegulation;
    delete [] labels;
}


void MainWindow::openFilenameDialog()
{
    //qDebug("MainWindow::openFilenameDialog()");
    filenameDialog->show();
}


void MainWindow::exportImage(QString filename)
{
    //qDebug()<<"MainWindow::exportImage("<<filename<<")";

    if(kochWidget->exportImage(filename))
        messageBox->setText(tr("Image has been saved succesfully."));
    else
        messageBox->setText(tr("Error - image has not been saved.\n"
                               "Maybe you forgot about the file\n"
                               "extension (like .png or .jpg)?"));

    messageBox->exec();
}
