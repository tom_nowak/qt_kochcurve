#pragma once

#include <QWidget>
#include <QString>

class KochCurve;


class KochWidget : public QWidget
{
    Q_OBJECT
public:
    explicit KochWidget(QWidget *parent = 0);
    ~KochWidget();
    bool exportImage(QString filename); // returns true if image has been saved

private:
    KochCurve *kochCurve;
    int colour [3]; // in RGB, unsigned char could be used, but there is int in all connected functions
    void paintEvent(QPaintEvent *);

public slots:
    void changeIterations(int);
    void changeCenterX(int);
    void changeCenterY(int);
    void changeRadius(int);
    void changeRotation(int);
    void changeRed(int);
    void changeGreen(int);
    void changeBlue(int);
};
