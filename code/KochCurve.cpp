#include "KochCurve.h"
#include <math.h>
#include <float.h>
#include <QDebug>

// Public functions:

KochCurve::KochCurve()
    : iterationsNumber(0), pointsList(), center(Point(100,100)), radius(100), rotation(0)
{
    // Three points - vertices of equilateral triangle.
    double rot = (rotation+240.0)*M_PI/180.0; // rotation in radians, the last point (rotated for 240 deg)
    pointsList.push_front(Point(radius*cos(rot) + center.x, radius*sin(rot) + center.y));

    rot -= 2.0*M_PI/3.0; // for another point - rotated for 120 degrees
    pointsList.push_front(Point(radius*cos(rot) + center.x, radius*sin(rot) + center.y));

    rot -= 2.0*M_PI/3.0; // for the first point
    pointsList.push_front(Point(radius*cos(rot) + center.x, radius*sin(rot) + center.y));

} // KochCurve::KochCurve()


void KochCurve::setIterationsNumber(int newIterationsNumber)
{
    int difference = newIterationsNumber - this->iterationsNumber;

    if(difference>0) // adding iterations
    {
        iterationsNumber = newIterationsNumber;
        for(int i=0; i<difference; ++i)
            this->addIteration();
        return;
    }

    if(difference==0) return;

    // Else - difference<0
    if(newIterationsNumber<0) return; // refuse to set something stupid
    iterationsNumber = newIterationsNumber;
    // Decrease iterationsNumber:
    for(int i=0; i>difference; --i)
        this->substractIteration();
    return;
} // void KochCurve::setIterationsNumber(int newIterationsNumber)


void KochCurve::setCenter(Point newCenter)
{
    float dx = newCenter.x - center.x;
    float dy = newCenter.y - center.y;
    center = newCenter;

    for(std::forward_list<Point>::iterator it = pointsList.begin(); it!=pointsList.end(); ++it)
    {
        it->x += dx;
        it->y += dy;
    }
} // void KochCurve::setCenter(Point newCenter)


void KochCurve::setCenterX(float newX)
{
    float dx = newX - center.x;
    center.x = newX;
    for(std::forward_list<Point>::iterator it = pointsList.begin(); it!=pointsList.end(); ++it)
        it->x += dx;
} // void KochCurve::setCenterX(float newX)


void KochCurve::setCenterY(float newY)
{
    float dy = newY - center.y;
    center.y = newY;
    for(std::forward_list<Point>::iterator it = pointsList.begin(); it!=pointsList.end(); ++it)
        it->y += dy;
} // void KochCurve::setCenterY(float newY)


void KochCurve::setRadius(float newRadius)
{
    if(newRadius <= FLT_EPSILON) return;
    float k = newRadius/radius;
    radius = newRadius;

    for(std::forward_list<Point>::iterator it = pointsList.begin(); it!=pointsList.end(); ++it)
    {
        it->x = (it->x - center.x)*k + center.x;
        it->y = (it->y - center.y)*k + center.y;
    }
} // void KochCurve::setRadius(float newRadius)


void KochCurve::setRotation(float newRotation)
{
    double dr = (newRotation-rotation)*M_PI/180.0; // in radians
    double sinus = sin(dr);
    double cosinus = cos(dr);
    rotation = newRotation;
    float nx, ny;

    for(std::forward_list<Point>::iterator it = pointsList.begin(); it!=pointsList.end(); ++it)
    {
        nx = it->x - center.x;
        ny = it->y - center.y;
        it->x = cosinus * nx - sinus * ny + center.x;
        it->y = sinus * nx + cosinus * ny + center.y;
    }
} // void KochCurve::setRotation(float newRotation)



//////////////////////////////////////////////////////////////////////////////////////////////////////
// Private functions:

void KochCurve::addSpike(PointIterator iter1, PointIterator iter2)
{
    // There is no data validation here (for iterators), because this is
    // a private function and it is called only when it should be.

    // Names of the variables below do not 'speak for themselves', they are just used in calculations.
    float dx = (iter2->x - iter1->x)/3.0;
    float sx = iter1->x + iter2->x;
    float dy = (iter2->y - iter1->y)/3.0;
    float sy = iter1->y + iter2->y;

    // Points sharing in three parts the line between it and the next point:
    pointsList.insert_after(iter1, Point(iter2->x - dx, iter2->y - dy));
    iter1 = pointsList.insert_after(iter1, Point(iter1->x + dx, iter1->y + dy));


    // The way of calculating coordinates of the 'spike' point is
    // the result of prior analysis - calculated on a piece of paper.

    static const float q = 3.0/(2.0*sqrt(3));
    dx *= q;
    sx /= 2.0;
    dy *= q;
    sy /= 2.0;

    pointsList.insert_after(iter1, Point(sx+dy, sy-dx));
} // void KochCurve::addSpike(PointIterator &iter1, PointIterator &iter2)


void KochCurve::addIteration()
{
    PointIterator iter1 = pointsList.begin();
    PointIterator iter2 = iter1;
    ++iter2;

    // Add spike between each pair of points in the list:
    while(iter2!=pointsList.end())
    {
        addSpike(iter1, iter2);
        iter1 = iter2;
        ++iter2;
    }

    // Add spike between the last and the first point in the list:
    iter2 = pointsList.begin();
    addSpike(iter1, iter2);
} // void KochCurve::addIteration()


void KochCurve::substractIteration()
{
    // Each iteration adds three points between each two form the previous iteration,
    // so to come back to the previous iteration, you need to delete each three
    // points after a point which is going to remain.
    for(PointIterator iter = pointsList.begin(); iter!=pointsList.end(); ++iter)
    {
        pointsList.erase_after(iter);
        pointsList.erase_after(iter);
        pointsList.erase_after(iter);
    }
} // void KochCurve::substractIteration()
