#pragma once

#include <QDialog>
#include <QString>


class QLineEdit;
class QPushButton;
class QLabel;
class QFile;

class FilenameDialog : public QDialog
{
    Q_OBJECT

public:
    FilenameDialog(QWidget *parent = 0);
    ~FilenameDialog();

private:
    QLineEdit *inputLine;
    QLabel *label;
    QLabel *warning;
    QPushButton *buttonOK;
    QPushButton *buttonCancel;
    QFile *predictedFile;

signals:
    void signalImageToSave(QString filename);

private slots:
    void fileExistenceChecker(QString);
    void handlerCancel();
    void handlerOK();
};
