#pragma once

#include <QMainWindow>
#include <QString>

class KochWidget;
class InterfaceWidget;
class QSlider;
class QSlider;
class QSpinBox;
class QLabel;
class FilenameDialog;
class QMessageBox;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    KochWidget *kochWidget; // shows the koch curve
    InterfaceWidget *interfaceWidget; // includes all the UI objects to manipulate the image
    FilenameDialog *filenameDialog; // asks for filename to save the image
    QMessageBox *messageBox; // tells if image has been saved

    QLabel *labels;
    QSpinBox *iterationsRegulation;
    QSlider *xCenterRegulation;
    QSlider *yCenterRegulation;
    QSlider *radiusRegulation;
    QSlider *rotationRegulation;
    QSlider *colourRegulation;

private slots:
    void openFilenameDialog();
    void exportImage(QString);
};
