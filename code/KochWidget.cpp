#include "KochWidget.h"
#include "KochCurve.h"
#include <QPainter>
#include <QColor>
#include <QDebug>
#include <QPixmap>

KochWidget::KochWidget(QWidget *parent) : QWidget(parent)
{
    kochCurve = new KochCurve;
    colour[0] = colour[1] = colour[2] = 0;
    this->setStyleSheet("background-color: white");
}


KochWidget::~KochWidget()
{
    delete kochCurve;
}


bool KochWidget::exportImage(QString filename)
{
    QPixmap pixmap(this->size());
    this->render(&pixmap);
    return pixmap.save(filename);
}


void KochWidget::paintEvent(QPaintEvent *event)
{
    //qDebug("KochWidget::paintEvent");

    QPainter qp(this);
    qp.setPen(QColor(colour[0], colour[1], colour[2]));

    PointIterator it1 = kochCurve->getIteratorBegin();
    PointIterator it2 = it1;
    ++it2; // it2 - always the next after it1
    PointIterator end = kochCurve->getIteratorEnd();

    int i=1; // to debug
    while(it2!=end)
    {
        qp.drawLine(it1->x, it1->y, it2->x, it2->y);
        ++it1;
        ++it2;
        ++i;
    }
    // After the loop it1 is one before end - pointer to the last point of the curve.
    // The last line connection the first and the last point needs to be drawn.
    it2 = kochCurve->getIteratorBegin();
    qp.drawLine(it1->x, it1->y, it2->x, it2->y);

    //qDebug()<<"lines drawn: "<<i;

    QWidget::paintEvent(event);
} // void KochWidget::paintEvent(QPaintEvent *event)


void KochWidget::changeIterations(int n)
{
    kochCurve->setIterationsNumber(n);
    this->repaint();
}

void KochWidget::changeCenterX(int x)
{
    kochCurve->setCenterX(static_cast<float>(x));
    this->repaint();
}

void KochWidget::changeCenterY(int y)
{
    kochCurve->setCenterY(static_cast<float>(y));
    this->repaint();
}

void KochWidget::changeRadius(int r)
{
    kochCurve->setRadius(static_cast<float>(r));
    this->repaint();
}

void KochWidget::changeRotation(int rot)
{
    kochCurve->setRotation(static_cast<float>(rot));
    this->repaint();
}

void KochWidget::changeRed(int cr)
{
    this->colour[0] = cr;
    this->repaint();
}

void KochWidget::changeGreen(int cg)
{
    this->colour[1] = cg;
    this->repaint();
}

void KochWidget::changeBlue(int cb)
{
    this->colour[2] = cb;
    this->repaint();
}
