#include "FilenameDialog.h"

#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QFile>


FilenameDialog::FilenameDialog(QWidget *parent)
    :QDialog(parent)
{
    setModal(true);
    setMinimumSize(350, 100);
    setMaximumSize(minimumSize());
    setWindowTitle(tr("Filename Dialog"));

    QGridLayout *layout = new QGridLayout(this);
    inputLine = new QLineEdit(this);
    label = new QLabel(tr("Output file name: "), this);
    label->setAlignment(Qt::AlignCenter);
    warning = new QLabel(this);
    warning->setAlignment(Qt::AlignCenter);
    buttonOK = new QPushButton(tr("OK"), this);
    buttonCancel = new QPushButton(tr("Cancel"), this);
    predictedFile = new QFile;

    layout->addWidget(label, 0, 0, 1, 1);
    layout->addWidget(inputLine, 0, 1, 1, 1);
    layout->addWidget(warning, 1, 0, 1, 2);
    layout->addWidget(buttonOK, 2, 0, 1, 1);
    layout->addWidget(buttonCancel, 2, 1, 1, 1);

    connect(buttonOK, SIGNAL(pressed()), this, SLOT(handlerOK()));
    connect(buttonCancel, SIGNAL(pressed()), this, SLOT(handlerCancel()));
    connect(inputLine, SIGNAL(textChanged(QString)), this, SLOT(fileExistenceChecker(QString)));
}


FilenameDialog::~FilenameDialog()
{
    delete predictedFile;
}


void FilenameDialog::fileExistenceChecker(QString currentText)
{
    predictedFile->setFileName(currentText);
    if(predictedFile->exists())
        warning->setText(tr("WARNIG - file already exists (it will be overwritten)."));
    else
        warning->setText(tr(""));
}


void FilenameDialog::handlerCancel()
{
    this->close();
    inputLine->clear();
}


void FilenameDialog::handlerOK()
{
    this->close();
    emit signalImageToSave(inputLine->text());
    inputLine->clear();
}
